import clients.SymBotClient;
import listeners.IMListener;
import model.InboundMessage;
import model.OutboundMessage;
import model.Stream;

public class IMListenerImpl implements IMListener {

    private SymBotClient botClient;

    public IMListenerImpl(SymBotClient botClient) {
        this.botClient = botClient;
    }

    public void onIMMessage(InboundMessage inboundMessage) {
        OutboundMessage messageOut = new OutboundMessage();
        String messageOutToOtherRoom = null;
        String otherRoomId = null;
        messageOut.setMessage("Hi "+inboundMessage.getUser().getFirstName()+"!, How are you doing!");
        if(!inboundMessage.getHashtags().isEmpty()) {
            for(String tag :inboundMessage.getHashtags()) {
                if(RoomListenerTestImpl.roomTagMap.containsKey(tag)) {
                    otherRoomId=RoomListenerTestImpl.roomTagMap.get(tag);
                    messageOutToOtherRoom="Question from "+inboundMessage.getUser().getFirstName()+": "+inboundMessage.getMessageText();
                    messageOut.setMessage("Your question has been sent to room "+otherRoomId);
                    continue;
                }
            }
        }
        try {
            this.botClient.getMessagesClient().sendMessage(inboundMessage.getStream().getStreamId(), messageOut);
            if(otherRoomId!=null && messageOutToOtherRoom!=null) {
                messageOut.setMessage(messageOutToOtherRoom);
                RoomListenerTestImpl.botClient.getMessagesClient().sendMessage(otherRoomId, messageOut);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onIMCreated(Stream stream) {

    }
}
